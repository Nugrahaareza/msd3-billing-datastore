package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.Organization;
import com.mii.msdtiga.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;

    public List<Organization> findByOrganizationContains(String orgName) {
        return organizationRepository.findByOrganizationNameContains(orgName);
    }

}
