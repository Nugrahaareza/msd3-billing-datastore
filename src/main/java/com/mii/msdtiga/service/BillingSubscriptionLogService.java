package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.BillingSubscriptionLog;
import com.mii.msdtiga.entity.Invoice;
import com.mii.msdtiga.repository.BillingSubscriptionLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillingSubscriptionLogService {

    @Autowired
    private BillingSubscriptionLogRepository billingSubscriptionLogRepository;

    public List<BillingSubscriptionLog> findByInvoice(Invoice invoice) {
        return billingSubscriptionLogRepository.findByInvoice(invoice);
    }

}
