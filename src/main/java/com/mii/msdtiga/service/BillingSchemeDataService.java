/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.BillingScheme;
import com.mii.msdtiga.entity.BillingSchemeCharge;
import com.mii.msdtiga.entity.Organization;
import com.mii.msdtiga.entity.Subscription;
import com.mii.msdtiga.repository.BillingSchemeChargeRepository;
import com.mii.msdtiga.repository.OrganizationRepository;
import com.mii.msdtiga.repository.SubscriptionRepository;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillingSchemeDataService {
    
    @Autowired
    private SubscriptionRepository subscriptionRepository;
    
    @Autowired
    private OrganizationRepository organizationRepository;
   
    @Autowired
    private BillingSchemeChargeRepository billingSchemeChargeRepository;
    
    private static final String FLAT = "FLAT";
    
    public String getBillingScehemeData(String organizationApimId){
        //get organizationId
        Organization organization = organizationRepository.findByOrgApimId(organizationApimId);
        //get subscription list
        List<Subscription> listSubscription = subscriptionRepository.findByOrganization(organization);
        JSONArray arrayInvoiceData = new JSONArray();
        for(int i=0; i<listSubscription.size();i++){
            //create object master
            JSONObject objectMaster= new JSONObject();
            objectMaster.put("organizationApimId", organization.getOrgApimId());
            objectMaster.put("organizationName", organization.getOrganizationName());
            JSONArray arrayDetail = new JSONArray();
            //get data billing scheme
            BillingScheme billingScheme = listSubscription.get(i).getBilllingscheme();         
            objectMaster.put("billingSchemeType", billingScheme.getBillingSchemeType());
            List<BillingSchemeCharge> listBillingSchemeCharge = billingSchemeChargeRepository.findByBillingPk(billingScheme);
            for(int j = 0;j<listBillingSchemeCharge.size();j++){
                //create object detail
               JSONObject objectDetail= new JSONObject();
               if(billingScheme.equals(FLAT)){
                   objectDetail.put("tierLevel", "");
                   objectDetail.put("minCall","");
                   objectDetail.put("maxCall","");
                   
               }else{
                   objectDetail.put("tierLevel", String.valueOf(listBillingSchemeCharge.get(j).getBillinglvl()));
                   objectDetail.put("minCall",listBillingSchemeCharge.get(j).getMinCall());
                   objectDetail.put("maxCall",listBillingSchemeCharge.get(j).getMaxCall());
               }
               objectDetail.put("billingCharge", listBillingSchemeCharge.get(j).getBillingCharge());
               arrayDetail.put(objectDetail);
            }
            //put array detail to master
            objectMaster.put("billingSchemeDetail", arrayDetail);
            //put array master to array invoice data
            arrayInvoiceData.put(objectMaster);
        }
        
        return arrayInvoiceData.toString();
    }
    
}
