package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.Invoice;
import com.mii.msdtiga.entity.Organization;
import com.mii.msdtiga.repository.BillingSubscriptionLogRepository;
import com.mii.msdtiga.repository.InvoiceRepository;
import com.mii.msdtiga.repository.OrganizationRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.mii.msdtiga.util.TraceLogger;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceDataService {

    Logger logger = TraceLogger.getLogger(getClass().getName());
    
    @Autowired
    private BillingSubscriptionLogRepository billingSubscriptionLogRepository;
    
    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    public Invoice find(long id) {
        return invoiceRepository.getOne(id);
    }

    public List<Invoice> getInvoice(LocalDate startDate, LocalDate endDate, String organizationName, Boolean paid) {
        List<Invoice> listInvoice = new ArrayList<>();
        List<Organization> organizations = new ArrayList<>();
        if(organizationName != null && organizationName.length() > 0) {
            organizations = organizationRepository.findByOrganizationNameContains(organizationName);
            for(Organization organization : organizations) {
                if(paid != null) {
                    listInvoice = invoiceRepository.findByDeletedAndOrganizationAndPaidAndInvoiceDateBetween(false, organization, paid, startDate, endDate);
                } else {
                    listInvoice = invoiceRepository.findByDeletedAndOrganizationAndInvoiceDateBetween(false, organization, startDate, endDate);
                }
            }
        } else {
            if(paid == null) {
                listInvoice = invoiceRepository.findByDeletedAndInvoiceDateBetween(false, startDate, endDate);
            } else {
                listInvoice = invoiceRepository.findByDeletedAndPaidAndInvoiceDateBetween(false, paid, startDate, endDate);
            }
        }
        return listInvoice;
    }
    
    /*public String getInvoiceData(String organizationApimId){
        
        //get organizationId
        Organization organization = organizationRepository.findByOrgApimId(organizationApimId);
        //get list invoice
        List<Invoice> listInvoice = invoiceRepository.findByOrganization(organization);
        JSONArray arrayInvoiceData = new JSONArray();
        for(int i=0; i<listInvoice.size();i++){
            //create object master
            JSONObject objectMaster= new JSONObject();
            objectMaster.put("organizationApimId", organization.getOrgApimId());
            objectMaster.put("organizationName", organization.getOrganizationName());
            objectMaster.put("virtualAccountNumber", listInvoice.get(i).getVirtualAccountNumber());
            objectMaster.put("periode", listInvoice.get(i).getYear()+"-"+String.format("%02d",listInvoice.get(i).getMonth()));
            objectMaster.put("invoiceTimestamp", listInvoice.get(i).getGenerateTimestamp().toString());
            objectMaster.put("totalInvoiceAmount", listInvoice.get(i).getTotalAmount().intValue());
                //find data invoice
                List<BillingSubscriptionLog> listBsl = billingSubscriptionLogRepository.findByOrganizationAndInvoice(organization, listInvoice.get(i));
                //create object array detail
                JSONArray arrayDetail = new JSONArray();
                for(int j=0;j<listBsl.size();j++){
                    //create object detail
                    JSONObject objectDetail= new JSONObject();
                    objectDetail.put("billingSchemeType", listBsl.get(j).getBillingSchemeType());
                    objectDetail.put("appsName", listBsl.get(j).getAppName());
                    objectDetail.put("productName", listBsl.get(j).getProductName());
                    objectDetail.put("totalUsage", listBsl.get(j).getUsageCount());
                    objectDetail.put("totalCharges",listBsl.get(j).getSubscriptionChargeTotal().intValue());
//                    //check scheme type
//                    if(listBsl.get(j).getBillingSchemeType().equals(FLAT)){
//                        objectDetail.put("usage", listBsl.get(j).getUsageCountFlat());
//                        objectDetail.put("charges", listBsl.get(j).getUsageChargeFlat().intValue());
//                        objectDetail.put("flatTotalCharges",listBsl.get(j).getSubscriptionChargeTotal());
//                    }else{
//                        objectDetail.put("usage", listBsl.get(j).getUsageCountFlat());
//                        objectDetail.put("charges", listBsl.get(j).getUsageChargeFlat().intValue());
//                        objectDetail.put("flatTotalCharges",listBsl.get(j).getSubscriptionChargeTotal());
//                    }
                    arrayDetail.put(objectDetail);
                }
                
            //put array detail to master
            objectMaster.put("Data", arrayDetail);
            //put array master to array invoice data
            arrayInvoiceData.put(objectMaster);
        }        
        return arrayInvoiceData.toString();
    }*/
}
