/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.Organization;
import com.mii.msdtiga.entity.Subscription;
import com.mii.msdtiga.entity.Usage;
import com.mii.msdtiga.repository.OrganizationRepository;
import com.mii.msdtiga.repository.SubscriptionRepository;
import com.mii.msdtiga.repository.UsageRepository;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsageDataService {
    
    @Autowired
    private UsageRepository usageRepository;
    
    @Autowired
    private SubscriptionRepository subscriptionRepository;
    
    @Autowired
    private OrganizationRepository organizationRepository;
    
    public String getUsageData(String organizationApimId){
        //get organizationId
        Organization organization = organizationRepository.findByOrgApimId(organizationApimId);
        //get subscription list
        List<Subscription> listSubscription = subscriptionRepository.findByOrganization(organization);
        JSONArray arrayInvoiceData = new JSONArray();
        for(int i=0; i<listSubscription.size();i++){
            //create object master
            JSONObject objectMaster= new JSONObject();
            objectMaster.put("organizationApimId", organization.getOrgApimId());
            objectMaster.put("organizationName", organization.getOrganizationName());
            objectMaster.put("appsName", listSubscription.get(i).getAppName());
            objectMaster.put("productName",listSubscription.get(i).getProductName());
            JSONArray arrayDetail = new JSONArray();
            //get data usage
            List<Usage> listUsage = usageRepository.findByUsageIdSubscription(listSubscription.get(i));
            for(int j = 0;j<listUsage.size();j++){
                //create object detail
               JSONObject objectDetail= new JSONObject();
               objectDetail.put("periode", listUsage.get(j).getUsageId().getPeriod().toString());
               objectDetail.put("usageCount",listUsage.get(j).getUsageCount());
               arrayDetail.put(objectDetail);
            }
            //put array detail to master
            objectMaster.put("usagePerDay", arrayDetail);
            //put array master to array invoice data
            arrayInvoiceData.put(objectMaster);
        }
        
        return arrayInvoiceData.toString();
    }
    
}
