package com.mii.msdtiga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//@EnableAutoConfiguration
@SpringBootApplication
public class ApiBIllingDatastoreApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ApiBIllingDatastoreApplication.class, args);
	}

}
