package com.mii.msdtiga.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mii.msdtiga.entity.BillingSubscriptionLog;
import com.mii.msdtiga.entity.Invoice;
import com.mii.msdtiga.service.*;
import com.mii.msdtiga.util.TraceLogger;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class BillingDatastoreController {
    
    
    private static final int HTTP_CODE_ERROR = 500;
    
    private static final String HTTP_DESC_ERROR = "Internal Error";
    
    Logger logger = TraceLogger.getLogger(getClass().getName());
    
    @Autowired
    private InvoiceDataService invoiceDataSvc;
    
    @Autowired
    private UsageDataService usageDataSvc;
    
    @Autowired
    private BillingSchemeDataService billingSchemeDataSvc;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private BillingSubscriptionLogService billingSubscriptionLogService;
    
    ObjectMapper jsonMapper = new ObjectMapper();

    @CrossOrigin
    @GetMapping(path="/invoice",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInvoice(@RequestParam String startDate,
                                                    @RequestParam String endDate,
                                                    @RequestParam(required = false) String organizationName,
                                                    @RequestParam(required = false) Boolean isPaid){
        LocalDate startLocalDate = LocalDate.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endLocalDate = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        List<Invoice> invoices = invoiceDataSvc.getInvoice(startLocalDate, endLocalDate, organizationName, isPaid);
        return ResponseEntity.ok(invoices);
    }

    @CrossOrigin
    @GetMapping(value = "/invoice/download", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<?> getInvoiceFile(@RequestParam long id) throws IOException {
        Invoice invoice = invoiceDataSvc.find(id);
        if(invoice== null) {
            return new ResponseEntity<String>("File not found", HttpStatus.NO_CONTENT);
        }
        File file = new File(invoice.getFilePath());

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+file.getName());
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    @CrossOrigin
    @GetMapping(path="/invoice/detail",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getInvoiceDetail(@RequestParam long id){
        Invoice invoice = invoiceDataSvc.find(id);
        List<BillingSubscriptionLog> billingSubscriptionLog = billingSubscriptionLogService.findByInvoice(invoice);
        return ResponseEntity.ok(billingSubscriptionLog);
    }
    
    /*
    @PostMapping(path="/getUsageData",consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},produces = "application/JSON")
    public ResponseEntity<String> getUsageData(@RequestParam MultiValueMap<String,String> paramMap){
        logger.debug("======================== New Incoming Request for getUsageData ========================");
        String organizationApimId = paramMap.getFirst("organizationApimId");
        String respPayload = usageDataSvc.getUsageData(organizationApimId);
        
        logger.debug("Response : " + respPayload);
        return ResponseEntity.ok().body(respPayload);
        
    }*
    
    /*
    @PostMapping(path="/getBillingSchemeData",consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE},produces = "application/JSON")
    public ResponseEntity<String> getBillingSchemeData(@RequestParam MultiValueMap<String,String> paramMap){
        logger.debug("======================== New Incoming Request for getBillingSchemeData ========================");
        String organizationApimId = paramMap.getFirst("organizationApimId");
        String respPayload = billingSchemeDataSvc.getBillingScehemeData(organizationApimId);
        
        logger.debug("Response : " + respPayload);
        return ResponseEntity.ok().body(respPayload);
        
    }
    */    
}
