package com.mii.msdtiga.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Embeddable
public class UsageId implements Serializable {

    @ManyToOne
    @JoinColumn
    private Subscription subscription;

    @Column
    @DateTimeFormat(pattern="yyyyMMdd")
    private LocalDate period;


    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    public LocalDate getPeriod() {
        return period;
    }

    public void setPeriod(LocalDate period) {
        this.period = period;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsageId usageId = (UsageId) o;
        return period == usageId.period &&
                Objects.equals(subscription, usageId.subscription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subscription, period);
    }
}
