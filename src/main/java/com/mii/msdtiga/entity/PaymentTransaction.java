/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mii.msdtiga.entity;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

//@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class PaymentTransaction {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String paymentBankReferenceNumber;

    @Column
    private String paymentReferenceId;

    @Column(columnDefinition = "DATE")
    private LocalDate paymentDate;

    @Column(columnDefinition = "TIME")
    private LocalTime paymentTime;

    @Column
    private LocalDateTime systemTimestamp;

    @Column
    private String paymentAuthorizationResponseId;

    @OneToOne
    @JoinColumn
    private Invoice invoice;
    
    @Column
    private BigDecimal paymentAmount;
    
    @Column
    private String virtualAccountNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaymentBankReferenceNumber() {
        return paymentBankReferenceNumber;
    }

    public void setPaymentBankReferenceNumber(String paymentBankReferenceNumber) {
        this.paymentBankReferenceNumber = paymentBankReferenceNumber;
    }

    public String getPaymentReferenceId() {
        return paymentReferenceId;
    }

    public void setPaymentReferenceId(String paymentReferenceId) {
        this.paymentReferenceId = paymentReferenceId;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }

    public LocalTime getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(LocalTime paymentTime) {
        this.paymentTime = paymentTime;
    }

    public LocalDateTime getSystemTimestamp() {
        return systemTimestamp;
    }

    public void setSystemTimestamp(LocalDateTime systemTimestamp) {
        this.systemTimestamp = systemTimestamp;
    }

    public String getPaymentAuthorizationResponseId() {
        return paymentAuthorizationResponseId;
    }

    public void setPaymentAuthorizationResponseId(String paymentAuthorizationResponseId) {
        this.paymentAuthorizationResponseId = paymentAuthorizationResponseId;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    /**
     * @return the paymentAmount
     */
    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    /**
     * @param paymentAmount the paymentAmount to set
     */
    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    /**
     * @return the virtualAccountNumber
     */
    public String getVirtualAccountNumber() {
        return virtualAccountNumber;
    }

    /**
     * @param virtualAccountNumber the virtualAccountNumber to set
     */
    public void setVirtualAccountNumber(String virtualAccountNumber) {
        this.virtualAccountNumber = virtualAccountNumber;
    }
    
    
}
