package com.mii.msdtiga.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "billing_subscription_log")
public class BillingSubscriptionLog implements Serializable{

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	@JoinColumn
	private Subscription subscription;

	@Column(name="month")
	private int month;

	@Column(name="year")
	private int year;

	@ManyToOne
	@JoinColumn
	private Organization organization;
	
	@Column(name = "org_name")
	private String orgName;
	
	@Column(name = "app_name")
	private String appName;
	
	@Column(name = "product_name")
	private String productName;

	@Column(name = "usage_count")
	private int usageCount;
	
	@Column(name = "billing_scheme_name")
	private String billingSchemeName;
	
	@Column(name = "billing_scheme_type")
	private String billingSchemeType;

	@Column(name = "usage_count_flat")
	private int usageCountFlat;
	
	@Column(name = "usage_charge_flat")
	private BigDecimal usageChargeFlat;
	
	@Column(name = "usage_subtotal_flat")
	private BigDecimal usageSubtotalFlat;
	
	@Column(name = "tier_length")
	private int tierLength;

	@Column(name = "usage_count_tier1")
	private int usageCountTier1;
	
	@Column(name = "usage_charge_tier1")
	private BigDecimal usageChargeTier1;
	
	@Column(name = "min_call_tier1")
	private int minCallTier1;
	
	@Column(name = "max_call_tier1")
	private int maxCallTier1;
	
	@Column(name = "usage_subtotal_tier1")
	private BigDecimal usageSubtotalTier1;

	@Column(name = "usage_count_tier2")
	private int usageCountTier2;
	
	@Column(name = "usage_charge_tier2")
	private BigDecimal usageChargeTier2;
	
	@Column(name = "min_call_tier2")
	private int minCallTier2;
	
	@Column(name = "max_call_tier2")
	private int maxCallTier2;
	
	@Column(name = "usage_subtotal_tier2")
	private BigDecimal usageSubtotalTier2;
	
	@Column(name = "usage_count_tier3")
	private int usageCountTier3;
	
	@Column(name = "usage_charge_tier3")
	private BigDecimal usageChargeTier3;
	
	@Column(name = "min_call_tier3")
	private int minCallTier3;
	
	@Column(name = "max_call_tier3")
	private int maxCallTier3;
	
	@Column(name = "usage_subtotal_tier3")
	private BigDecimal usageSubtotalTier3;
	
	@Column(name = "usage_count_tier4")
	private int usageCountTier4;
	
	@Column(name = "usage_charge_tier4")
	private BigDecimal usageChargeTier4;
	
	@Column(name = "min_call_tier4")
	private int minCallTier4;
	
	@Column(name = "max_call_tier4")
	private int maxCallTier4;
	
	@Column(name = "usage_subtotal_tier4")
	private BigDecimal usageSubtotalTier4;
	
	@Column(name = "usage_count_tier5")
	private int usageCountTier5;
	
	@Column(name = "usage_charge_tier5")
	private BigDecimal usageChargeTier5;
	
	@Column(name = "min_call_tier5")
	private int minCallTier5;
	
	@Column(name = "max_call_tier5")
	private int maxCallTier5;
	
	@Column(name = "usage_subtotal_tier5")
	private BigDecimal usageSubtotalTier5;
	
	@Column(name = "subscription_charge_total")
	private BigDecimal subscriptionChargeTotal;

	@ManyToOne
	@JoinColumn(nullable = true)
	private Invoice invoice;

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getUsageCount() {
		return usageCount;
	}

	public void setUsageCount(int usageCount) {
		this.usageCount = usageCount;
	}

	public String getBillingSchemeName() {
		return billingSchemeName;
	}

	public void setBillingSchemeName(String billingSchemeName) {
		this.billingSchemeName = billingSchemeName;
	}

	public String getBillingSchemeType() {
		return billingSchemeType;
	}

	public void setBillingSchemeType(String billingSchemeType) {
		this.billingSchemeType = billingSchemeType;
	}

	public int getUsageCountFlat() {
		return usageCountFlat;
	}

	public void setUsageCountFlat(int usageCountFlat) {
		this.usageCountFlat = usageCountFlat;
	}

	public BigDecimal getUsageChargeFlat() {
		return usageChargeFlat;
	}

	public void setUsageChargeFlat(BigDecimal usageChargeFlat) {
		this.usageChargeFlat = usageChargeFlat;
	}

	public BigDecimal getUsageSubtotalFlat() {
		return usageSubtotalFlat;
	}

	public void setUsageSubtotalFlat(BigDecimal usageSubtotalFlat) {
		this.usageSubtotalFlat = usageSubtotalFlat;
	}

	public int getTierLength() {
		return tierLength;
	}

	public void setTierLength(int tierLength) {
		this.tierLength = tierLength;
	}

	public int getUsageCountTier1() {
		return usageCountTier1;
	}

	public void setUsageCountTier1(int usageCountTier1) {
		this.usageCountTier1 = usageCountTier1;
	}

	public BigDecimal getUsageChargeTier1() {
		return usageChargeTier1;
	}

	public void setUsageChargeTier1(BigDecimal usageChargeTier1) {
		this.usageChargeTier1 = usageChargeTier1;
	}

	public BigDecimal getUsageSubtotalTier1() {
		return usageSubtotalTier1;
	}

	public void setUsageSubtotalTier1(BigDecimal usageSubtotalTier1) {
		this.usageSubtotalTier1 = usageSubtotalTier1;
	}

	public int getUsageCountTier2() {
		return usageCountTier2;
	}

	public void setUsageCountTier2(int usageCountTier2) {
		this.usageCountTier2 = usageCountTier2;
	}

	public BigDecimal getUsageChargeTier2() {
		return usageChargeTier2;
	}

	public void setUsageChargeTier2(BigDecimal usageChargeTier2) {
		this.usageChargeTier2 = usageChargeTier2;
	}

	public BigDecimal getUsageSubtotalTier2() {
		return usageSubtotalTier2;
	}

	public void setUsageSubtotalTier2(BigDecimal usageSubtotalTier2) {
		this.usageSubtotalTier2 = usageSubtotalTier2;
	}

	public int getUsageCountTier3() {
		return usageCountTier3;
	}

	public void setUsageCountTier3(int usageCountTier3) {
		this.usageCountTier3 = usageCountTier3;
	}

	public BigDecimal getUsageChargeTier3() {
		return usageChargeTier3;
	}

	public void setUsageChargeTier3(BigDecimal usageChargeTier3) {
		this.usageChargeTier3 = usageChargeTier3;
	}

	public BigDecimal getUsageSubtotalTier3() {
		return usageSubtotalTier3;
	}

	public void setUsageSubtotalTier3(BigDecimal usageSubtotalTier3) {
		this.usageSubtotalTier3 = usageSubtotalTier3;
	}

	public int getUsageCountTier4() {
		return usageCountTier4;
	}

	public void setUsageCountTier4(int usageCountTier4) {
		this.usageCountTier4 = usageCountTier4;
	}

	public BigDecimal getUsageChargeTier4() {
		return usageChargeTier4;
	}

	public void setUsageChargeTier4(BigDecimal usageChargeTier4) {
		this.usageChargeTier4 = usageChargeTier4;
	}

	public BigDecimal getUsageSubtotalTier4() {
		return usageSubtotalTier4;
	}

	public void setUsageSubtotalTier4(BigDecimal usageSubtotalTier4) {
		this.usageSubtotalTier4 = usageSubtotalTier4;
	}

	public int getUsageCountTier5() {
		return usageCountTier5;
	}

	public void setUsageCountTier5(int usageCountTier5) {
		this.usageCountTier5 = usageCountTier5;
	}

	public BigDecimal getUsageChargeTier5() {
		return usageChargeTier5;
	}

	public void setUsageChargeTier5(BigDecimal usageChargeTier5) {
		this.usageChargeTier5 = usageChargeTier5;
	}

	public BigDecimal getUsageSubtotalTier5() {
		return usageSubtotalTier5;
	}

	public void setUsageSubtotalTier5(BigDecimal usageSubtotalTier5) {
		this.usageSubtotalTier5 = usageSubtotalTier5;
	}

	public BigDecimal getSubscriptionChargeTotal() {
		return subscriptionChargeTotal;
	}

	public void setSubscriptionChargeTotal(BigDecimal subscriptionChargeTotal) {
		this.subscriptionChargeTotal = subscriptionChargeTotal;
	}
	
	public Organization getOrganization() {
		return organization;
	}

	public void setOrg(Organization organization) {
		this.organization = organization;
	}

	public int getMinCallTier1() {
		return minCallTier1;
	}

	public void setMinCallTier1(int minCallTier1) {
		this.minCallTier1 = minCallTier1;
	}

	public int getMaxCallTier1() {
		return maxCallTier1;
	}

	public void setMaxCallTier1(int maxCallTier1) {
		this.maxCallTier1 = maxCallTier1;
	}

	public int getMinCallTier2() {
		return minCallTier2;
	}

	public void setMinCallTier2(int minCallTier2) {
		this.minCallTier2 = minCallTier2;
	}

	public int getMaxCallTier2() {
		return maxCallTier2;
	}

	public void setMaxCallTier2(int maxCallTier2) {
		this.maxCallTier2 = maxCallTier2;
	}

	public int getMinCallTier3() {
		return minCallTier3;
	}

	public void setMinCallTier3(int minCallTier3) {
		this.minCallTier3 = minCallTier3;
	}

	public int getMaxCallTier3() {
		return maxCallTier3;
	}

	public void setMaxCallTier3(int maxCallTier3) {
		this.maxCallTier3 = maxCallTier3;
	}

	public int getMinCallTier4() {
		return minCallTier4;
	}

	public void setMinCallTier4(int minCallTier4) {
		this.minCallTier4 = minCallTier4;
	}

	public int getMaxCallTier4() {
		return maxCallTier4;
	}

	public void setMaxCallTier4(int maxCallTier4) {
		this.maxCallTier4 = maxCallTier4;
	}

	public int getMinCallTier5() {
		return minCallTier5;
	}

	public void setMinCallTier5(int minCallTier5) {
		this.minCallTier5 = minCallTier5;
	}

	public int getMaxCallTier5() {
		return maxCallTier5;
	}

	public void setMaxCallTier5(int maxCallTier5) {
		this.maxCallTier5 = maxCallTier5;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
}
