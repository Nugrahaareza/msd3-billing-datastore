package com.mii.msdtiga.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "api_usage")
public class Usage implements Serializable {

    @EmbeddedId
    private UsageId usageId;

    @Column
    private int usageCount;

    public UsageId getUsageId() {
        return usageId;
    }

    public void setUsageId(UsageId usageId) {
        this.usageId = usageId;
    }

    public int getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(int usageCount) {
        this.usageCount = usageCount;
    }
}
