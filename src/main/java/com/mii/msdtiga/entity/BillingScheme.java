package com.mii.msdtiga.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "billing_scheme")
public class BillingScheme implements Serializable{
	
	@Id
	@Column(name = "id")
        private long id;
	
	@Column(name = "scheme_name")
	private String schemeName;
	
	@Column(name = "scheme_type")
	private String schemeType;
	
	@Column(name = "approval_status")
	private int schemeApprovalStatus;

	public long getBillingSchemeId() {
		return id;
	}

	public void setBillingSchemeId(long id) {
		this.id = id;
	}

	public String getBillingSchemeName() {
		return schemeName;
	}

	public void setBillingSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	public String getBillingSchemeType() {
		return schemeType;
	}

	public void setBillingSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}

	public int getBillingSchemeApprovalStatus() {
		return schemeApprovalStatus;
	}

	public void setBillingSchemeApprovalStatus(int schemeApprovalStatus) {
		this.schemeApprovalStatus = schemeApprovalStatus;
	}
	
	
	
}
