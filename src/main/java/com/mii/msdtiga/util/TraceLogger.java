/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mii.msdtiga.util;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("traceLogger")
public class TraceLogger {

	private static Map<String, Logger> loggerMap = new LinkedHashMap<String, Logger>();

	public static void debug(String message, String category) {
		getLogger(category).debug(message);
	}

	public static void info(String message, String category) {
		getLogger(category).info(message);
	}

	public static void error(String message, String category) {
		getLogger(category).error(message);
	}

	public static void error(String message, String category, Exception e) {
		getLogger(category).error(message, e);
	}

	public static Logger getLogger(String category) {
		if (loggerMap.get(category) == null) {
			loggerMap.put(category, LogManager.getLogger(category));
		}
		return loggerMap.get(category);
	}

}
