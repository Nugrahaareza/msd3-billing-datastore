package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {

    Organization findByOrgApimId(String organizationName);

    List<Organization> findByOrganizationNameContains(String organizationName);

}
