package com.mii.msdtiga.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mii.msdtiga.entity.BillingScheme;
import com.mii.msdtiga.entity.BillingSchemeCharge;

@Repository
public interface BillingSchemeChargeRepository extends JpaRepository<BillingSchemeCharge, Long> {

	List<BillingSchemeCharge> findByBillingPk(BillingScheme bs);
}
