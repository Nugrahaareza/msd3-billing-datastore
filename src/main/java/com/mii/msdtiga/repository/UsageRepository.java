package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.Subscription;
import com.mii.msdtiga.entity.Usage;
import com.mii.msdtiga.entity.UsageId;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsageRepository extends JpaRepository<Usage, UsageId> {

    public List<Usage> findByUsageIdSubscription(Subscription subscription);
	
}
