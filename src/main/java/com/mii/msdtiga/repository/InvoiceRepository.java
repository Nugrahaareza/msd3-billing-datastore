package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.Invoice;
import com.mii.msdtiga.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
    
    public List<Invoice> findByDeletedAndOrganizationAndInvoiceDateBetween(boolean deleted, Organization organization, LocalDate startDate, LocalDate endDate);

    public List<Invoice> findByDeletedAndInvoiceDateBetween(boolean deleted, LocalDate startDate, LocalDate endDate);

    public List<Invoice> findByDeletedAndPaidAndInvoiceDateBetween(boolean deleted, boolean paid, LocalDate startDate, LocalDate endDate);

    public List<Invoice> findByDeletedAndOrganizationAndPaidAndInvoiceDateBetween(boolean deleted, Organization organization, boolean paid, LocalDate startDate, LocalDate endDate);

}
